# Java data types

## Learning outcomes
* getting to know Java primitive types
* getting to know arrays
* basic programming (with `for`)
* Object-oriented design

## Assignments

1. In class `Datatypes`, work your way through the methods and implement them according to the instructions 
stated within the methods' Javadoc and/or in the method body.

2. \[This assignment does not have JUnit tests\] Create a Java program within this package that reads 
command-line arguments describing animals to include in a zoo. Each commandline argument represents two properties of
 an animal: species name and age. The commandline call of your program will have this form:
 `chimpanzee:14 giraffe:20 lion:4 lion:18 chimpanzee:11`
 After processing the commandline, you should  
 
     (a) print the number of animals in the zoo   
     (b) print the average age of the animals    
     (c) print each animal (properties) by calling a method that you created for this purpose.   
      
While creating this app you should pay attention to
 
 - create the correct classes with the correct names
 - define the correct instance variables (properties) with well chosen names

Hint: have a look at the String class on how to split strings into elements.